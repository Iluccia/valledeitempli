﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValleDeiTempli
{
    class Persona
    {
        private string nome,
                       residenza,
                       professione;
        private int    annoDiNascita,
                       altezza;

        //costruttore vuoto
        //public persona() {}

        //Costruttore
        public Persona(string nome, string residenza, string professione, int annoDiNascita, int altezza)
        {
            //this.Nome = Nome;       il this è una chiave
            Nome = nome;
            Residenza = residenza;
            Professione = professione;
            AnnoDiNascita = annoDiNascita;
            Altezza = altezza;
        }

        public string Nome
        {
            get
            {
                return nome;

            }

            set
            {
                if (value != "" && value != null)
                    nome = value;
                else
                    nome = "errore";
            }


        }

        public string Residenza
        {
            get
            {
                return residenza;

            }

            set
            {
                //if (value != "" && value != null)
                if (Strumenti.CittaItaliana(value.ToLower()))
                    residenza = value;
                else
                    residenza = "errore";
            }
        }
        public string Professione
        {
            get
            {
                return professione;

            }

            set
            {
                if (value != "" && value != null)
                    professione = value;
                else
                    professione = "errore";
            }

        }
        public int AnnoDiNascita
        {

            get
            {
                return annoDiNascita;

            }

            set
            {
                //if (value > 1905 && value <= 2017)
                if (value > 1905 && value <= Strumenti.AnnoCorrente)
                    annoDiNascita = value;
                else
                    annoDiNascita = 0;
            }

        }
        public int Altezza
        {

            get
            {
                return altezza;

            }

            set
            {
                if (value > 50 && value < 280)
                    altezza = value;
                else
                    altezza = 0;
            }
        }
        public int Eta()
        {
            return Strumenti.AnnoCorrente - annoDiNascita;

        }

        public int Eta(int annoCorrente)
        {

            return annoCorrente - annoDiNascita;

        }

        //public static int Eta(int annoDiNascita)
        //{
        //    return Strumenti.AnnoCorrente - annoDiNascita;

        //}

        public bool Valida()
        {
            return
            nome != "errore"
            &&
            residenza != "errore"
            &&
            professione != "errore"
            &&
            annoDiNascita != 0
            &&
            altezza != 0;
        }
    }

}
