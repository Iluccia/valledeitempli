﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValleDeiTempli
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Persona> persone = new List<Persona>();

            //Persona p = new Persona();  si usa quando il costruttore public persona e vuoto
            //Persona p = new Persona("");

            //p.Nome = "Pippo";
            //p.Professione = "Nullafacente";
            //p.Residenza = "Milano";

            //Console.WriteLine(p.Nome + " " + p.Professione + " " + p.Residenza);

            Persona p = new Persona("Alex", "Milano", "studente", 1994, 184);
            Persona p1 = new Persona("Marco", "Milano", "studente", 1994, 188);
            Persona p2 = new Persona("Ilaria", "Milano", "studente", 1993, 167);
            Persona p3 = new Persona("Daniele", "Agrigento", "studente", 1992, 172);
            Persona p4 = new Persona("Matteo", "Milano", "professore", 1981, 200);

            persone.Add(p);
            persone.Add(p1);
            persone.Add(p2);
            persone.Add(p3);
            persone.Add(p4);

            int prezzo,
                    sconto = 5;

            foreach (Persona persona in persone)
            {
                //prezzo = 20;
                //sconto = 0;

                //variabile = condizione ? A : B;

                //Un Metodo :
                //prezzo = 20;
                //sconto = 0;

                //sconto += persona.Eta() < 9 || persona.Eta() > 70 ? (prezzo - sconto) : prezzo;

                //sconto += (persona.Eta() < 9 || persona.Eta() > 70) ? 5 : 0;
                //sconto += (persona.Residenza.ToLower() == Strumenti.cittaItaliane[1] || persona.Residenza.ToLower() == Strumenti.cittaItaliane[3]) ? 5 : 0;
                //sconto += (persona.Professione == "studente" || persona.Professione == "professore") ? 5 : 0;
                //sconto += (persona.Altezza < 90) ? 5 : 0;

                //Secondo Metodo:
                prezzo = 20;

                sconto =
                ((persona.Eta() < 9 || persona.Eta() > 70) ? 5 : 0)
                +
                ((persona.Residenza.ToLower() == Strumenti.cittaItaliane[1] || persona.Residenza.ToLower() == Strumenti.cittaItaliane[3]) ? 5 : 0)
                +
                ((persona.Professione == "studente" || persona.Professione == "professore") ? 5 : 0)
                +
                ((persona.Altezza < 90) ? 5 : 0);

                // Terzo Metodo :
                //prezzo = 20;
                //sconto = 0;

                //sconto += (persona.Altezza < 90) ? (prezzo - sconto) : prezzo;


                //if (persona.Eta() < 9 || persona.Eta() > 70)
                //    sconto += 20;

                //if (persona.Residenza.ToLower() == Strumenti.cittaItaliane[1] || persona.Residenza.ToLower() == Strumenti.cittaItaliane[3])
                //    sconto += 20;

                //if (persona.Professione == "studente" || persona.Professione == "professore")
                //    sconto += 10;

                //if (persona.Altezza < 90)
                //    sconto += 20;

                //if (sconto > 20)
                //    sconto = 20;

                Console.WriteLine("Il Sig. " + persona.Nome + " paga " + (prezzo - sconto) + " euro");

            }


            //int prezzo = 20,
            //    sconto = 0;

            //if (p.Eta() < 9 || p.Eta() > 70)
            //    sconto += 20;

            //if (p.Residenza == Strumenti.cittaItaliane[1] || p.Residenza == Strumenti.cittaItaliane[3])
            //    sconto += 20;

            //if (p.Professione == "studente" || p.Professione == "professore")
            //    sconto += 10;

            //if (p.Altezza < 90)
            //    sconto += 20;

            //if (sconto > 20)
            //    sconto = 20;

            //Console.WriteLine(p.Valida());

            Console.ReadLine();

        }

    }

}
