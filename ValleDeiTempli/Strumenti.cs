﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValleDeiTempli
{
    class Strumenti
    {
        public static string[] cittaItaliane = new string[] { "milano", "agrigento", "roma", "palermo" };
        //public static int AnnoCorrente = 2017;
        public static int AnnoCorrente = DateTime.Today.Year;

        public static bool CittaItaliana(string citta)
        {
            bool risposta = false;

            //for (int i = 0; i < cittaItaliane.Length; i++)
            //{
            //if (citta == cittaItaliane[i])
            //    risposta = true;

            //}

            foreach (string s in cittaItaliane)
            {
                if (citta == s)
                    risposta = true;
            }

            //Metodo con While:
            //int k = 0;

            //while (k < cittaItaliane.Length)
            //{
            //    if (citta == cittaItaliane[k])
            //        risposta = true;

            //}

            //Metodo con Do:
            //k = 0;
            //do
            //{
            //    if (citta == cittaItaliane[k])
            //        //        risposta = true;
            //        k++;

            //} while (k < cittaItaliane.Length);


            //while (1 > 0)
            //{

            //}

            //while (true)
            //{

            //}

            ////do
            ////{

            ////}
            ////while (1 > 0);

            //int contatore = 0;

            //do
            //{
            //    contatore++;
            //}
            //while (contatore < 10);

            return risposta;

        }

    }
}
